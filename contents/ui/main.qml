/*
 *  Copyright 2014 Martin Klapetek <mklapetek@kde.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  2.010-1301, USA.
 */

import QtQuick 2.0
import org.kde.plasma.core 2.0 as PlasmaCore


Rectangle {
    id: main
    width: 800
    height: 600
    color: "#000000"

    Text {
        id: text
        color: "white"
        font.pointSize: 72
        anchors.centerIn: parent
    }

    Text {
        id: colorText
        color: "white"
        font.pointSize: 24
        anchors.top: text.bottom
        anchors.horizontalCenter: text.horizontalCenter
        anchors.topMargin: text.height
    }

        PlasmaCore.DataSource {
        id: dataSource
        engine: "time"
        connectedSources: "Local"
        interval: 1000
        onDataChanged: {
            var date = new Date(data["Local"]["DateTime"]);
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var seconds = date.getSeconds();

            if (hours < 10) {
                hours = "0" + hours;
            }

            if (minutes < 10) {
                minutes = "0" + minutes;
            }

            if (seconds < 10) {
                seconds = "0" + seconds;
            }

            main.color = "#" + hours + minutes + seconds;
            text.text = hours + ":" + minutes + ":" + seconds;
            colorText.text = main.color
        }
        Component.onCompleted: {
            onDataChanged();
        }
    }

}

